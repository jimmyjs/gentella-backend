<?php

use App\User;
use Faker\Factory;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create('id_ID');
        $faker->addProvider(new Faker\Provider\Base($faker));
    	$count41 = 0;
    	$count42 = 0;
    	$count51 = 0;
    	$count52 = 0;
    	$count10 = 0;

        // create random 20 general users
        for ($i = 0; $i < 10; $i++) {
        	$digits = 16;
            $email = strtolower($faker->email);
            $year = ['2011','2012', '2013', '2014'];
            $studyProgramId = [1,2,3,4,5,6,7,8,9,10];
            $religion = ['Kristen Protestan', 'Kristen Katholik', 'Islam', 'Buddha'];
            $region = ['Jakarta', 'Aceh', 'Papua Barat', 'Pontianak', 'Medan', 'Manado', 'Palu'];
            $twoDigitNim = ['41','42','51','52','10'];
            $generatedtgn = $faker->randomElement($twoDigitNim);
            $count = 0;
            if($generatedtgn == '41') {
            	$count41 ++;
            	$count = $count41;
            } else if($generatedtgn == '42') {
            	$count42 ++;
            	$count = $count42;
            } else if($generatedtgn == '51') {
            	$count51 ++;
            	$count = $count51;
            } else if($generatedtgn == '52') {
            	$count52 ++;
            	$count = $count52;
            } else if($generatedtgn == '10') {
            	$count10 ++;
            	$count = $count10;
            }
            $generatedYear = $faker->randomElement($year);
            User::create([
                'nim' => $generatedtgn.$generatedYear.str_pad($count, 3, '0', STR_PAD_LEFT),
                'study_program_id' => $faker->randomElement($studyProgramId),
                'password' => bcrypt('123456'),
                'first_name' => $faker->firstName(($i % 2 == 0) ? 'male' : 'female'),
                'last_name' => $faker->lastName,
                'gender' => ($i % 2 == 0) ? 'male' : 'female',
                'id_number' => rand(pow(10, $digits-1), pow(10, $digits)-1),
                'entry_year' => $generatedYear,
                'phone' => $faker->phoneNumber,
                'email' => $email,
                'date_birth' => $faker->dateTimeBetween('-25 years', '-18 years')->format('Y-m-d'),
                'date_place' => $faker->randomElement($region),
                'religion' => $faker->randomElement($religion),
                'address' => $faker->address,
                'is_admin' => 0
            ]);
        }

        User::create([
            'nim' => '111111111',
            'study_program_id' => '',
            'password' => bcrypt('123456'),
            'first_name' => $faker->firstName('male'),
            'last_name' => $faker->lastName,
            'gender' => 'male',
            'id_number' => rand(pow(10, $digits-1), pow(10, $digits)-1),
            'entry_year' => $generatedYear,
            'phone' => $faker->phoneNumber,
            'email' => 'admin@admin.com',
            'date_birth' => $faker->dateTimeBetween('-35 years', '-28 years')->format('Y-m-d'),
            'date_place' => $faker->randomElement($region),
            'religion' => $faker->randomElement($religion),
            'address' => $faker->address,
            'is_admin' => 1
        ]);
    }
}
