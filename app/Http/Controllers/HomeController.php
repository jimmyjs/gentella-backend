<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Session;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Session::flash('notif_success', 'alert-hello'); 
        return view('backend.dashboard');
    }

    public function table()
    {
        // Session::flash('notif_success', 'alert-hello'); 
        return view('backend.table');
    }
}
